import React, { useContext } from "react"
import moment from "moment"
import { Button, Checkbox, Table } from "semantic-ui-react"
import { Link } from "react-router-dom"

import NotesContext from "../context/NotesContext"

const Note = ({ note }) => {
	const { notesDispatch } = useContext(NotesContext)
	const handleChange = (e, { checked, id }) => {
		notesDispatch({type: "SET_EXCUSED", id: id, excused: checked})
	}
	return (
		<Table.Row>
			<Table.Cell collapsing>
				{/* <Checkbox toggle  onClick={() => notesDispatch({type: "MARK_EXCUSED", id: note.id})} /> */}
				<Checkbox 
					toggle
					id={note.id}
					checked={note.excused}
					onChange={handleChange}     
				/>
			</Table.Cell>
			<Table.Cell>{note.student}</Table.Cell>
			<Table.Cell>{note.course}</Table.Cell>
			<Table.Cell>{moment(note.date).format("DD.MM.YYYY")}</Table.Cell>
			<Table.Cell>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore.</Table.Cell>
			<Table.Cell collapsing style={{background: "rgba(0,0,0,.03)"}}>
			<Link to={`/edit/${note.id}`}><Button compact primary circular icon="edit" /></Link>
				<Button compact negative circular icon="close" onClick={() => notesDispatch({type: "REMOVE_NOTE",  id: note.id})} />
			</Table.Cell>
		</Table.Row>
	)
}

export { Note as default }