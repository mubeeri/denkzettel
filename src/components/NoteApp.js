import React, { useEffect, useReducer } from "react"

import AppRouter from "../routers/AppRouter"
import notesReducer from "../reducers/notes"
import filterReducer from "../reducers/filters"

import NotesContext from "../context/NotesContext"

const NoteApp = () => {
	const [notes, notesDispatch] = useReducer(notesReducer, [])
	const [filter, filterDispatch] = useReducer(filterReducer, { course: "" })

	useEffect(() => {
		const notes = JSON.parse(localStorage.getItem("notes"))

		if (notes) {
			notesDispatch({ type: "POPULATE_NOTES", notes })
		}
	}, [])

	useEffect(() => {
		localStorage.setItem("notes", JSON.stringify(notes))
	}, [notes])

	return (
		<NotesContext.Provider value={{ notes, notesDispatch, filter, filterDispatch }}>
			<AppRouter />
		</NotesContext.Provider>
	)
}

export { NoteApp as default }