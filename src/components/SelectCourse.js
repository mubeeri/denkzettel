import React, { useContext } from "react"
import NotesContext from "../context/NotesContext"


const SelectCourse = () => {
	const { filter, filterDispatch } = useContext(NotesContext)

	const setCourseFilter = (course) => {
		filterDispatch({
			type: "SET_COURSE",
			course
		})
	}

	return (
		<label>
			<select
				value={filter.course}
				onChange={(e) =>  setCourseFilter(e.target.value)}
			>
				<option value="">Bitte wählen</option>
				<option value="Mathematik">Mathematik</option>
				<option value="Englisch">Englisch</option>
				<option value="Französisch">Französisch</option>
				<option value="Deutsch">Deutsch</option>
			</select>
		</label>
	)
}

export { SelectCourse as default }