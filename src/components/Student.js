import React, { useState } from "react"

const Student = ({ name }) => {
	const [test, setTest] = useState(true)

	return (
		<label style={{textDecoration: test ? "none" : "line-through"}}>
			<input 
				type="checkbox"
				value={name}
				defaultChecked={true}
				onChange={(e) => setTest(e.target.checked)}
				name="student"
			/> {name}
		</label>
	)
}

export { Student as default }