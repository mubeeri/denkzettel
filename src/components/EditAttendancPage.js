import React, { useState, useEffect, useContext } from "react"
import uuid from "uuid"
import moment from "moment"
import { SingleDatePicker } from "react-dates"
import { Button, Form } from "semantic-ui-react"

import NotesContext from "../context/NotesContext"
import Layout from "./Layout"


const EditAttendancPage = (props) => {
	const [createdAt, setCreatedAt] = useState(moment())
	const [calendarFocused, setCalendarFocused] = useState(false)
	const { notes, notesDispatch } = useContext(NotesContext)

	const [id, setId] = useState("")
	const [course, setCourse] = useState("")
	const [student, setStudent] = useState("")
	const [noteText, setNoteText] = useState("")

	useEffect(() => {
		const noteToEdit = notes.find((note) => note.id === props.match.params.id)
		if (noteToEdit) {
			setId(noteToEdit.id)
			setCourse(noteToEdit.course)
			setStudent(noteToEdit.student)
			// setCreatedAt(noteToEdit.date)
		}
	},[notes])
	
	const options = [
		{ key: "Mathematik", text: "Mathematik", value: "Mathematik" },
		{ key: "Englisch", text: "Englisch", value: "Englisch" },
		{ key: "Französisch", text: "Französisch", value: "Französisch" },
		{ key: "Deutsch", text: "Deutsch", value: "Deutsch" },
	  ]
	return (
		<Layout title="Absenz bearbeiten">
			<Form>
				<Form.Group>
					<label>Datum</label>
					<SingleDatePicker
						date={createdAt}
						onDateChange={date => setCreatedAt(date)}
						focused={calendarFocused.focused}
						onFocusChange={focusedInput => setCalendarFocused(focusedInput)}
						id={uuid()}
						numberOfMonths={1}
						isOutsideRange={() => false}
					/>
				</Form.Group>
				<Form.Field>
					<label>Name</label>
					<input placeholder="Name des Schülers" value={student} onChange={null} />
				</Form.Field>
				<Form.Select fluid label="Klasse" options={options} value={course} placeholder="Klasse wählen"  onChange={null} />
				
				<Form.TextArea label="Notiz" placeholder="..." value={noteText}  onChange={null} />
				<Button type="submit">Speichern</Button>
			</Form>
		</Layout>
	)
}
export { EditAttendancPage as default }