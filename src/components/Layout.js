import React, { useState } from "react"
import { NavLink } from "react-router-dom"

import { Container, Grid, Header, Menu, Responsive, Sidebar, Button, Icon } from "semantic-ui-react"

// Semantic UI React Menu pointing won"t change active state on Route change
// https://stackoverflow.com/questions/47333168/semantic-ui-react-menu-pointing-wont-change-active-state-on-route-change
const Layout = ({ title, children }) => {
	const [sidebarShow, setSidebarShow] = useState(false)

	return (
		<Sidebar.Pushable>
			<Sidebar
				as={Menu}
				animation="overlay"
				icon="labeled"
				inverted
				onHide={() => setSidebarShow(false)}
				vertical
				visible={sidebarShow}
				width="thin"
			>
				<Menu.Item>
					<Icon name="sidebar" />
				</Menu.Item>
				<Menu.Item
					name="Dashboard"
					as={NavLink}
					to="/"
					exact={true}
				/>
				<Menu.Item
					name="Absenzen"
					as={NavLink}
					to="/absenzen"
				/>
				<Menu.Item
					name="Noten"
					as={NavLink}
					to="/noten"
				/>
				<Menu.Item
					name="Verhalten"
					as={NavLink}
					to="/verhalten"
				/>
				<Menu.Item
					name="Hilfe"
					as={NavLink}
					to="/hilfe"
				/>
				<Menu.Item
					name="Logout"
					as={NavLink}
					to="/login"
				/>
			</Sidebar>
			
			<Sidebar.Pusher dimmed={sidebarShow}>
				<Container style={{ marginTop: "2em", marginBottom: "1em" }}>
					<Header as="h1" textAlign="center">Denkzettel</Header>  
					
					<Responsive maxWidth={767}>
						<Button.Content onClick={() => setSidebarShow(true)}>
							<Icon name="sidebar" size="large" />
						</Button.Content>
					</Responsive>

					<Responsive minWidth={768}>
						<Menu pointing secondary stackable>
							<Menu.Item
								name="Dashboard"
								as={NavLink}
								to="/"
								exact={true}
							/>
							<Menu.Item
								name="Absenzen"
								as={NavLink}
								to="/absenzen"
							/>
							<Menu.Item
								name="Noten"
								as={NavLink}
								to="/noten"
							/>
							<Menu.Item
								name="Verhalten"
								as={NavLink}
								to="/verhalten"
							/>

							<Menu.Menu position="right">
								<Menu.Item
									name="Hilfe"
									as={NavLink}
									to="/hilfe"
								/>
								<Menu.Item
									name="Logout"
									as={NavLink}
									to="/login"
								/>
							</Menu.Menu>
						</Menu>
					</Responsive>

					<Header as="h2" dividing>
						{title}
					</Header>


					<Grid stackable>
						<Grid.Column>
							{children}
						</Grid.Column>
					</Grid>

				</Container>
			</Sidebar.Pusher>
		</Sidebar.Pushable>


	)
}

export { Layout as default }