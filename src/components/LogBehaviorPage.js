import React from "react"

import Layout from "./Layout"

const LogBehaviorPage = () => (
	<Layout title="Seite zum erfassen des Verhaltens"></Layout>
)

export { LogBehaviorPage as default }