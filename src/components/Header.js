import React from "react"
import { NavLink } from "react-router-dom"

const isActiveStyle = {
	fontWeight: "bold"
}

const Header = () => (
	<header>
		<h1>Denkzettel</h1>
		<NavLink to="/" activeStyle={isActiveStyle} exact={true}>Übersicht</NavLink> |&nbsp;
		<NavLink to="/attendance" activeStyle={isActiveStyle}>Absenzen</NavLink> |&nbsp; 
		<NavLink to="/grade" activeStyle={isActiveStyle}>Noten</NavLink> |&nbsp; 
		<NavLink to="/behavior" activeStyle={isActiveStyle}>Verhalten</NavLink> |&nbsp; 
		<NavLink to="/help" activeStyle={isActiveStyle}>Hilfe</NavLink>
	</header>
)
export { Header as default }