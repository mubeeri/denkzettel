import React from "react"

import Layout from "./Layout"


const HelpPage = () => (
	<Layout title="Wie bediene ich die Denkzettel App?"></Layout>
)

export { HelpPage as default }