import React, { useContext } from "react"
import moment from "moment"

import Note from "./Note"
import NotesContext from "../context/NotesContext"

// React State with Hooks: useReducer, useState, useContext
// https://www.robinwieruch.de/react-state-usereducer-usestate-usecontext/
const NoteList = () => {
	const { notes, filter } = useContext(NotesContext)

	const filteredCourse = notes.filter(note => {
		if (filter.course === "") {
		  return true
		}

		if (filter.course === note.course) {
			return true
		}

		return false
	  })

	const filteredExcused = filteredCourse.filter(note => {
		if (filter.excused === undefined) {
			return true
		}
		if (filter.excused === note.excused) {
			return true
        }
        return false
	})
	const filteredDate = filteredExcused.filter(note => {
		const dateFilter = moment(filter.startDate).unix() < moment(note.date).unix() && moment(filter.endDate).unix() > moment(note.date).unix()
		if (filter.startDate && filter.endDate && !dateFilter) {
			return false
		}
			return true
	})


	return filteredDate.map((note) => (
		<Note key={note.id} note={note}/>
	))
}

export { NoteList as default }