import React, { useState, useContext } from "react"
import uuid from "uuid"
import moment from "moment"

import NotesContext from "../context/NotesContext"
import { SingleDatePicker } from "react-dates"
import Student from "./Student"

// React Router - How to pass History object to a component 
// https://dev.to/kozakrisz/react-router---how-to-pass-history-object-to-a-component-3l0j
const Students = ({ props }) => {
	const [formData, setFormData] = useState()
	const [createdAt, setCreatedAt] = useState(moment())
	const [calendarFocused, setCalendarFocused] = useState(false)
	const { notesDispatch, filter } = useContext(NotesContext)


	const students = [
		{ id: "1", name: "Samuel A." },
		{ id: "2", name: "Jiri B." },
		{ id: "3", name: "Janik C." },
		{ id: "4", name: "Laura D." },
		{ id: "5", name: "Nils E." },
		{ id: "6", name: "Livia F." }
	]
	

	// React Forms: Using Refs  
	// https://css-tricks.com/react-forms-using-refs/
	const addNote = (e) => {
		e.preventDefault()
		const { student } = formData
		const checkboxArray = Array.prototype.slice.call(student)
		const checkedCheckboxes = checkboxArray.filter(input => !input.checked)
		// console.log("checked array:", checkedCheckboxes)
		const checkedCheckboxesValues = checkedCheckboxes.map(input => input.value)
		// console.log("checked array values:", checkedCheckboxesValues)
		checkedCheckboxesValues.forEach((student) => {
			notesDispatch({
				type: "ADD_NOTE",
				id: uuid(),
				course: filter.course, 
				student: student,
				date: createdAt,
				timestamp: moment().unix()
			})
		})
		props.history.push("/")
	}

	return (
		<form onSubmit={addNote} ref={(form) => setFormData(form)}>
			<SingleDatePicker
				date={createdAt}
				onDateChange={date => setCreatedAt(date)}
				focused={calendarFocused.focused}
				onFocusChange={focusedInput => setCalendarFocused(focusedInput)}
				id={uuid()}
				numberOfMonths={1}
				isOutsideRange={() => false}
			/>
			{students.map((student) => (
				<Student key={student.id} name={student.name}/>
			))}
			<button>Erfassen</button>
		</form>
	)
}

export { Students as default }