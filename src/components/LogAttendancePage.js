import React, { useContext } from "react"

import NotesContext from "../context/NotesContext"

import Layout from "./Layout"
import SelectCourse from "./SelectCourse"
import Students from "./Students"


const LogAttendancePage = (props) => {
	const { filter } = useContext(NotesContext)
	return (
		<Layout title="Seite zum erfassen der Absenzen">
			<SelectCourse />
			{ filter.course && <Students props={props} /> }
		</Layout>
	)
}

export { LogAttendancePage as default }