import React, { useState, useContext } from "react"
import { Button, Dropdown, Header, Icon, List, Table } from "semantic-ui-react"
import { DateRangePicker } from "react-dates"

import NotesContext from "../context/NotesContext"

import Layout from "./Layout"
import NoteList from "./NoteList"
// import AddNoteForm from "./AddNoteForm"
// import SelectCourse from "./SelectCourse"

const NoteDashboardPage = () => {
	const [startDate, setStartDate] = useState(null)
	const [endDate, setEndDate] = useState(null)
	const [calendarFocused, setCalendarFocused] = useState(null)
	const { notesDispatch, filterDispatch } = useContext(NotesContext)

	const onDatesChange = ({ startDate, endDate }) => {
		setStartDate(startDate)
		setEndDate(endDate)
		filterDispatch({
			type: "SET_START_DATE",
			startDate
		})
		filterDispatch({
			type: "SET_END_DATE",
			endDate
		})
	}

	const setCourseFilter = (course) => {
		filterDispatch({
			type: "SET_COURSE",
			course
		})
	}
	const setExcusedFilter = (excused) => {
		filterDispatch({
			type: "FILTER_EXCUSED",
			excused
		})
	}
	// const excuseAll = () => {
	// 	notesDispatch({type: "ALL_EXCUSED"})
	// }
	return(
		<Layout title="Seite zur Übersicht der Einträge ">
			<List horizontal relaxed>
				<List.Item>
					<List.Content>
					<Dropdown text="Filtern nach Fach" icon="filter" floating labeled button className="icon">
						<Dropdown.Menu>
							<Dropdown.Header icon="tags" content="Fach auwählen" />
							<Dropdown.Divider />
							<Dropdown.Item onClick={() =>  setCourseFilter("")}>Alle</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setCourseFilter("Mathematik")}>Mathematik</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setCourseFilter("Englisch")}>Englisch</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setCourseFilter("Französisch")}>Französisch</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setCourseFilter("Deutsch")}>Deutsch</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
					</List.Content>
				</List.Item>
				<List.Item>
					<List.Content>
					<Dropdown text="Filtern nach Entschuldigt" icon="filter" floating labeled button className="icon">
						<Dropdown.Menu>
							<Dropdown.Header icon="tags" content="Bitte wählen" />
							<Dropdown.Divider />
							<Dropdown.Item onClick={() =>  setExcusedFilter(undefined)}>Alle</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setExcusedFilter(false)}>Unentschultig</Dropdown.Item>
							<Dropdown.Item onClick={() =>  setExcusedFilter(true)}>Entschultig</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
					</List.Content>
				</List.Item>
				<List.Item>
					<List.Content>
						<List.Header>Filter nach Datum:</List.Header>
						<DateRangePicker
							startDate={startDate}
							startDateId="your_unique_start_date_id"
							endDate={endDate}
							endDateId="your_unique_end_date_id"
							onDatesChange={({ startDate, endDate }) => onDatesChange({ startDate, endDate })}
							focusedInput={calendarFocused}
							onFocusChange={focusedInput => setCalendarFocused(focusedInput)}
							showClearDates={true}
							numberOfMonths={1}
							isOutsideRange={() => false}
						/>
					</List.Content>
				</List.Item>
			</List>


			<Header as="h3" dividing>
				Tabelle mit Absenzen
			</Header>
			<Table celled definition>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell />
						<Table.HeaderCell>Name</Table.HeaderCell>
						<Table.HeaderCell>Kurs</Table.HeaderCell>
						<Table.HeaderCell>Datum</Table.HeaderCell>
						<Table.HeaderCell>Notiz</Table.HeaderCell>
						<Table.HeaderCell style={{background: "0 0"}} />
					</Table.Row>
				</Table.Header>

				<Table.Body>
					<NoteList/>
				</Table.Body>

				{/* <Table.Footer fullWidth>
					<Table.Row>
						<Table.HeaderCell />
						<Table.HeaderCell colSpan="4">
							<Button floated="right" icon labelPosition="left" primary size="small">
								<Icon name="user" /> Einzelne Absenz erfassen
							</Button>
							<Button size="small"  onClick={excuseAll}>Alle entschuldigen</Button>
						</Table.HeaderCell>
						<Table.HeaderCell />
					</Table.Row>
				</Table.Footer> */}

			</Table>
			<Header as="h3" dividing>
				Tabelle mit Noten
			</Header>
			<Header as="h3" dividing>
				Tabelle mit Verhalten
			</Header>
			{/* <AddNoteForm/> */}
		</Layout>
	)
}

export { NoteDashboardPage as default }