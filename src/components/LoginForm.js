import React from "react"
import { Button, Form, Grid, Message, Segment } from "semantic-ui-react"
import { Link } from "react-router-dom"

const LoginForm = () => (
	<Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
		<Grid.Column style={{ maxWidth: 450 }}>
			
			<Form size="large">
				<Segment>
					<Form.Input fluid icon="user" iconPosition="left" placeholder="E-Mailadresse" />
					<Form.Input
						fluid
						icon="lock"
						iconPosition="left"
						placeholder="Passwort"
						type="password"
					/>

					<Link to="/"><Button fluid size="large">Login</Button></Link>
				</Segment>
			</Form>

			<Message>
				Noch keine Login? Hier können Sie sich <Link to="/">registrieren</Link>
			</Message>
		</Grid.Column>
	</Grid>
)

export default LoginForm
