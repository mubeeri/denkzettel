import React from "react";
import ReactDOM from "react-dom";
// import "normalize.css/normalize.css";
// import "sanitize.css/sanitize.css";
import "semantic-ui-css/semantic.min.css"
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import * as serviceWorker from "./serviceWorker";

import NoteApp from "./components/NoteApp"

ReactDOM.render(<NoteApp />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
