import React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"

import NoteDashboardPage from "../components/NoteDashboardPage"
import LogAttendancePage from "../components/LogAttendancePage"
import EditAttendancPage from "../components/EditAttendancPage"
import LogGradePage from "../components/LogGradePage"
import LogBehaviorPage from "../components/LogBehaviorPage"
import HelpPage from "../components/HelpPage"
import NotFoundPage from "../components/NotFoundPage"
import LoginForm from "../components/LoginForm";

const AppRouter = () => (
	<BrowserRouter>
		<Switch>
			<Route path="/" component={NoteDashboardPage} exact={true}/>
			<Route path="/absenzen" component={LogAttendancePage}/>
			<Route path="/edit/:id" component={EditAttendancPage} />
			<Route path="/noten" component={LogGradePage}/>
			<Route path="/verhalten" component={LogBehaviorPage}/>
			<Route path="/hilfe" component={HelpPage}/>
			<Route path="/login" component={LoginForm}/>
			<Route component={NotFoundPage}/>
		</Switch>
	</BrowserRouter>
)

export { AppRouter as default }