const notesReducer = (state, action) => {
	switch(action.type) {
		case "POPULATE_NOTES":
			return action.notes
		case "ADD_NOTE":
			return [
				...state,
				{ 
					id: action.id,
					course: action.course, 
					student: action.student,
					date: action.date,
					timestamp: action.timestamp,
					excused: false
				}
			]
		case "SET_EXCUSED":
			return state.map((note) => {
				if (note.id === action.id) {
					return {
						...note,
						excused: action.excused
					}
				} else {
					return note;
				}
			})
		
		case "ALL_EXCUSED":
			return state.map((note) => {
				return {
					...note,
					excused: true
				}
			})
		case "REMOVE_NOTE":
			return state.filter((note) => note.id !== action.id)
		default:
			return state
	}
}

export { notesReducer as default }